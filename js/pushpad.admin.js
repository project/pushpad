(function ($) {

/**
 * Provide the summary information for the type_configuration settings vertical tabs.
 */
Drupal.behaviors.type_configurationSettingsSummary = {
  attach: function (context) {
    // Make sure this behavior is processed only if drupalSetSummary is defined.
    if (typeof jQuery.fn.drupalSetSummary == 'undefined') {
      return;
    }
  }
};

})(jQuery);
