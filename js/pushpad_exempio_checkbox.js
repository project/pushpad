// (function ($) {
//   Drupal.behaviors.Pushpad= {
//     attach: function (context, settings) {
        

        //alert ("qwewq");
        pushpad('init', 3478);

        // get some information about the current subscription:
        // we use it to initialize the checkboxes
        pushpad('status', function (isSubscribed, tags) {

            // for each checkbox
            $('.push-preference').each(function () {
                var element = $(this);
                var tag = element.data('tag');

                // initialize the checkbox to the correct status: checked / unchecked
                if (isSubscribed && tags.indexOf(tag) != -1) {
                element.prop('checked', true);
                } else {
                element.prop('checked', false);
                }

                // listen for events on the checkbox
                element.on('change', function (e) {
                e.preventDefault();

                // when the checkbox is checked you must subscribe to push notifications
                if(element.is(':checked')) {
                    // we'll check the checkbox again when we are absolutely sure that 
                    // the subscription is successful
                    element.prop('checked', false);

                    pushpad('subscribe', function (isSubscribed) {
                    if (isSubscribed) {
                        element.prop('checked', true);
                    } else {
                        alert('You have blocked the notifications for this website. Please change your browser preferences and try again.');
                    }
                    }, { tags: [tag] });

                // when the checkbox is unchecked you must unsubscribe from push notifications
                } else {
                    pushpad('unsubscribe', function () {
                    element.prop('checked', false);
                    }, { tags: [tag] });
                }
                });
            });

        });


//     }
//   }
// }
