<?php

/**
 * @file
 * Admin interface for the Pushpad Morning Notification Module
 * @author: Augusto Fagioli <http://drupal.org/user/50680>
 *
 *
 */


/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function pushpad_examples_morning_notification_admin_settings_form($form_state) {

  global $base_url ;

  $form['pmn'] = array(
	'#type' => 'fieldset',
	'#title' => t('Pushpad morning notification'),
	'#description' => t('Send a daily notification to your subscribers'),
  );

  
  
  
  $form['related_modules'] = Array(
	  '#type' => 'fieldset',
	  '#title' => t('Related modules'),
	  '#description' => t('This modules relys also on the !l configuration ', Array("!l" => l("Pushpad Module", "admin/config/services/pushpad"))),
  );  
  
  
  
  $test_now = "";
  if (  variable_get('pmn_active', 0) == 1 )  {
    $test_now = pushpad_examples_morning_notification_test_link() ;
  }
  
  
  
  $form['pmn']['pmn_active'] = Array(
	'#type' => 'radios',
	'#title' => t('Turn ON/OFF'),
	'#options' => Array(
	  1  => t("On") ,
	  0  => t("Off"), 
	),
	'#default_value' => variable_get('pmn_active', 1),
	'#description' => t("Turn ON/OFF Pushpad notifications sent from this website. " . $test_now),
  );  
  
  // Visibility settings.
	//   $form['type_configuration'] = array( //type_configuration
	//     '#type' => 'item',
	//     '#title' => t('Type configuration'),
	//   );
    
  $form['pmn_tabs'] = Array( //type_configuration
	'#type' => 'vertical_tabs',
	'#attached' => Array(
	  'js' => array(drupal_get_path('module', 'pushpad') . '/js/pushpad.admin.js'),
	),
  );

  // ------------------------------------------
  // - A
  $form['pmn_tabs']['pmn_tab_time'] = array(
	'#type'   => 'fieldset',
	'#title'  => t('Time'),
	'#description' => t('Time settings'),
	'#weight' => 0 ,
  );      
  
   
  $form['pmn_tabs']['pmn_tab_time']['pmn_days'] = array(
	'#title' => t('Days'),
	'#type' => 'checkboxes',
	'#description' => t('Select days of the week for which you want notification'),
	'#default_value' => pushpad_examples_morning_notification_get_selected_days(), 
	'#options' => array(
	  '1' => t('Monday'),
	  '2' => t('Tuesday'),
	  '3' => t('Wednesday'),
	  '4' => t('Thursday'),
	  '5' => t('Friday'),
	  '6' => t('Saturday'),
	  '7' => t('Sunday'), 
	) ,
  );
    
  $form['pmn_tabs']['pmn_tab_time']['pmn_hour'] = Array(
	'#title' => t("Hour") ,
	'#description' => t('Select the time notification will be sent. You need to set up cron.'),
	'#type' => 'select',
	'#default_value' => variable_get('pmn_hour', ''),
	'#required' => TRUE,
	'#options' => Array(
	  '1'       =>  t('1am')       ,  
	  '2'       =>  t('2am')       ,  
	  '3'       =>  t('3am')       ,  
	  '4'       =>  t('4am')       ,
	  '5'       =>  t('5am')       ,  
	  '6'       =>  t('6am')       ,  
	  '7'       =>  t('7am')       ,  
	  '8'       =>  t('8am')       ,
	  '9'       =>  t('9am')       ,
	  '10'      =>  t('10am')      ,
	  '11'      =>  t('11am')      ,
	  '12'      =>  t('12am')      ,
	  '13'      =>  t('1pm')       ,
	  '14'      =>  t('2pm')       ,
	  '15'      =>  t('3pm')       ,
	  '16'      =>  t('4pm')       ,
	  '17'      =>  t('5pm')       ,
	  '18'      =>  t('6pm')       ,
	  '19'      =>  t('7pm')       ,
	  '20'      =>  t('8pm')       ,
	  '21'      =>  t('9pm')       ,
	  '22'      =>  t('10pm')      ,
	  '23'      =>  t('11pm')      ,
	  '24'      =>  t('midnight')  ,
	),
  );   
   
  $form['pmn_tabs']['pmn_tab_time']['pmn_wait_for'] = Array(
	'#title' => t('Wait for'),
	'#description' => t("between notifications.<br/>You don't want to send web notifications too often. Your subscribers may be not happy. Stay away from too-often notifications with this time setting."),
	'#type' => 'select',
	'#default_value' => variable_get('pmn_wait_for', 82800 ),
	'#required' => FALSE,
	'#options' => Array(
	  '0'       =>  t('No time check. For testing poupose')       ,
	  '3600'    =>  t('1 hour')                                   ,
	  '7200'    =>  t('2 hours')                                  ,
	  '10800'   =>  t('3 hours')                                  ,
	  '14400'   =>  t('4 hours')                                  ,
	  '18000'   =>  t('5 hours')                                  ,
	  '21600'   =>  t('6 hours')                                  ,
	  '43200'   =>  t('12 hours')                                 ,
	  '82800'   =>  t('23 hours')                                 ,
	  '86400'   =>  t('1 day')                                    ,
	  '172800'  =>  t('2 days')                                   ,
	),
  );   
   

  //  TODO as per main module
  //  $form['pmn_tabs']['pmn_tab_time']['pmn_ttl'] = Array(
  //	'#title' => t('Expire'),
  //	'#description' => t('Select in how many time notification will expire. if you are not sure abot what to do, leave the default !l', Array("!l" => l("Pushpad settings", "https://pushpad.xyz/projects"))),
  //	'#type' => 'select',
  //	'#default_value' => variable_get('pmn_ttl', variable_get('pushpad_general_ttl') ),
  //	'#required' => FALSE,
  //	'#options' => Array(  
  //	  '0'       =>  t('Pushpad default')   ,
  //	  '60'      =>  t('1 minute')          ,
  //	  '900'     =>  t('15 minutes')        ,
  //	  '1800'    =>  t('30 minutes')        ,
  //	  '3600'    =>  t('1 hour')            ,
  //	  '5400'    =>  t('90 minutes')        ,
  //	  '7200'    =>  t('2 hours')           ,
  //	  '10800'   =>  t('3 hours')           ,
  //	  '14400'   =>  t('4 hours')            ,
  //	  '18000'   =>  t('5 hours')           ,
  //	  '21600'   =>  t('6 hours')           ,
  //	  '43200'   =>  t('12 hors')           ,
  //	  '86400'   =>  t('1 day')             ,
  //	),
   // );


  // - B
  // Render some General configuration fields.
  $form['pmn_tabs']['pmn_tab_subscribe'] = array(
	'#type' => 'fieldset',
	'#title' => t('Subscribe'),
	'#description' => t('Labels and setting for the Subscribe block. This is what your subscribers see when subscribing.'),
	'#weight' => 1 ,
  );             
  
  $form['pmn_tabs']['pmn_tab_subscribe']['pmn_s_title'] = Array(
	'#title' => t('Title'),
	'#type' => 'textfield',
	'#default_value' => variable_get('pmn_s_title', t('Subscribe to Morning notification')),
	'#size' => 40,
	'#maxlength' => 40,
	'#required' => true,
	'#description' => t('This is title of the subscription block'), 
  );         
      
  $form['pmn_tabs']['pmn_tab_subscribe']['pmn_s_description'] = Array(
	'#title' => t('Description'),
	'#type' => 'textfield',
	'#default_value' => variable_get('pmn_s_description', t('Activate your preference and receive notification from us')),
	'#size' => 80,
	'#maxlength' => 80,
	'#required' => true,
	'#description' => t('This is the 	title of the subscription block'), 
  );               
   
   
   
  $tags_desciption = t("Tags can will be used to filter your subscribers. 
  <br/>Example: set up 'FREE' and 'PAY' for a website offering both free and 'on-payment' services. You will be then able to send 
  Free services to subscribers who selected the 'FREE' tag.
  
  
  "); 
   
  $form['pmn_tabs']['pmn_tab_subscribe']['tags'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tags '),
    '#description' => $tags_desciption, //t('Tags Up to 2 tags can be set here. Only fill this if you know what you are doing. More details on !link' , Array("!link" => l("pushpad docs", "https://pushpad.xyz/docs/tags") )),
  );       
   
   
  $form['pmn_tabs']['pmn_tab_subscribe']['tags'][0] = array(
    '#type' => 'fieldset',
    '#title' => t('Tag #1'),
    '#description' => t('A Pushpad Morning Notification will sent to subscriber to this TAG'),
  );          
	$form['pmn_tabs']['pmn_tab_subscribe']['tags'][0]['pmn_tag_1'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Tag #1'),
	  '#description' => t('Tag as set up on pushpad Panel'),
	  '#default_value' => variable_get('pmn_tag_1', t('some-tag')),
	  '#maxlength' => 12,
	  '#required' => true,
	  
	); 
	$form['pmn_tabs']['pmn_tab_subscribe']['tags'][0]['pmn_label_1'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Label #1'),
	  '#description' => t('Label tag as set up on pushpad Panel'),
	  '#default_value' => variable_get('pmn_label_1', t('some-label')),
	  '#maxlength' => 24,
	  '#required' => true,
	);   
   
  $form['pmn_tabs']['pmn_tab_subscribe']['tags'][1] = array(
    '#type' => 'fieldset',
    '#title' => t('Tag #2'),
    '#description' => t('You will be able to notify these subscribers from ' , Array("!l" => l("Pushpad Panel", "https://pushpad.xyz/"))),
  );          
	$form['pmn_tabs']['pmn_tab_subscribe']['tags'][1]['pmn_tag_2'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Tag #2'),
	  '#description' => t('Tag as set up on pushpad Panel'),
	  '#default_value' => variable_get('pmn_tag_2', t('some-tag')),
	  '#maxlength' => 12,
	); 
	$form['pmn_tabs']['pmn_tab_subscribe']['tags'][1]['pmn_label_2'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Label #2'),
	  '#description' => t('Label tag as set up on pushpad Panel'),
	  '#default_value' => variable_get('pmn_label_2', t('some-label')),
	  '#maxlength' => 24,
	);
   
   
   
   
   
  // ---------------------------------------------
  // - C
  $form['pmn_tabs']['pmn_tab_notification'] = array(
	'#type' => 'fieldset',
	'#title' => t('Notify'),
	'#description' => t('Labels and setting for the Morning Notification. This is what your subscribers will receive as notification.'),
	'#weight' => 2,
  );           
   
  
  $form['pmn_tabs']['pmn_tab_notification']['pmn_body'] = Array(
	'#title' => t('Body'),
	'#type' => 'textfield',
	'#default_value' => variable_get('pmn_body', ''),
	'#size' => 80,
	'#maxlength' => 120,
	'#required' => true,
	'#description' => t('This is the notification message'), 
  );         
   
  $form['pmn_tabs']['pmn_tab_notification']['pmn_title'] = Array(
	'#title' => t('Title'),
	'#type' => 'textfield',
	'#default_value' => variable_get('pmn_title', variable_get('site_name', 'Drupal') ),
	'#size' => 30,
	'#maxlength' => 30,
	'#required' => false,
	'#description' => t('You can add a title to your notification'), 
  );
	
  $form['pmn_tabs']['pmn_tab_notification']['pmn_target_url'] = Array(
	'#title' => t('Link'),
	'#type' => 'textfield',
	'#default_value' => variable_get('pmn_target_url', $base_url  ),
	'#required' => false,
	'#description' => t('Add a valid link as notification target. No validation'), 
  );

	
  $form['pmn_tabs']['pmn_tab_notification']['pmn_icon_url'] = Array(
	'#title' => t('Icon'),
	'#type' => 'textfield',
	'#default_value' => variable_get('pmn_icon_url', theme_get_setting('logo') ),
	'#required' => false,
	'#description' => t('Add a valid custom icon URL for your notification. No validation'), 
  );

  
  
  if (variable_get('pmn_active')== 0)  {
    drupal_set_message(t("Pushpad Morning Notification is DISABLED"), "warning", $repeat = FALSE);
  }
  return system_settings_form($form);
} 
 
 

/**
 * Implements _form_validate().
 */
function pushpad_examples_morning_notification_admin_settings_form_validate($form, &$form_state) {

}

/**
 * Implements _form_submit().
 */
function pushpad_examples_morning_notification_admin_settings_form_submit($form, &$form_state) {

  //  buggy drupal_set_message
  //  https://drupal.stackexchange.com/a/24111/13760
  $_SESSION['messages'] = '';

} 
