<?php

/**
 * @file
 * Admin interface for the Pushpad Drupal Module
 * @author: Augusto Fagioli <http://drupal.org/user/50680>
 *
 *
 */


/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function pushpad_admin_settings_form($form_state) {

  global $is_https ;
  
  $form['pushpad_active'] = Array(
	  '#type' => 'radios',
	  '#title' => t('Turn ON/OFF'),
	  '#options' => Array(
		1  => t("On") ,
		0  => t("Off"), 
	  ),
	  '#default_value' => variable_get('pushpad_active', 0),
	  '#description' => t("Turn ON/OFF Pushpad notifications sent from this website"),
  );  

  if ($is_https == TRUE)  {
	$https_status = t("Your website is running over HTTPS. Pushpad PRO will run smootly :) ");
  }
  else {
	$https_status = t(" Sorry :(  NO HTTPS detected. Pushpad PRO will not run here :( ");
	
	if (variable_get('pushpad_active_way', 0) == "pro")  {
	  drupal_set_message ("Pushpad Pro can't run without HTTPS. Check the both Web server and Pushpad Module configurations", "warning" , $repeat = FALSE); 
	}
  }
   
  $form['type'] = array(
	'#type' => 'fieldset',
	'#title' => t('Active type'),
	'#description' => t('Pushpad offers two different ways for collecting subscriptions to push notifications. Depending on your needs you may want to use one or the other.'),
  );
   
  $form['type']['pushpad_active_way'] = Array(
	  '#type' => 'radios',
	  '#title' => t('Active Way of notification'),
	  '#options' => Array(
		"express" => "Pushpad Express - NOT SUPPORTED, yet", // it would be nice to expose this option as disabled
		"pro"     => t('Pushpad Pro') . " - " . $https_status,
	  ),
	  '#default_value' => variable_get('pushpad_active_way', 0),
	  '#description' => t('See  !l', Array("!l" => l("documentation", "https://pushpad.xyz/docs") )),
	  //'#suffix' => t("Pushpad Express is not supported"),
  );
    
  // Visibility settings.
  $form['type_configuration'] = array( //type_configuration
    '#type' => 'item',
    '#title' => t('Type configuration'),
  );
    
  $form['type_configuration'] = Array( //type_configuration
	'#type' => 'vertical_tabs',
	'#attached' => Array(
	  'js' => array(drupal_get_path('module', 'pushpad') . '/js/pushpad.admin.js'),
	),
  );

    
  //  - A 
  // Render some General configuration fields.
  $form['type_configuration']['general'] = array(
	'#type' => 'fieldset',
	'#title' => t('General'),
  );    
    

  $form['type_configuration']['general']['pushpad_auth_token'] = Array(
    '#title' => t('Access token'),
    '#type' => 'textfield',
    '#default_value' => variable_get('pushpad_auth_token', ''),
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('Get your access token from the !l', Array("!l" => l("PushPad Contron Panel", "https://pushpad.xyz/users/edit")  )),
    '#weight'  => -99 ,
  );        
      

  $options = Array(
    '0'       =>  t('Pushpad default')   ,  
    '60'      =>  t('1 minute')          ,  
    '900'     =>  t('15 minutes')        ,  
    '1800'    =>  t('30 minutes')        ,  
    '3600'    =>  t('1 hour')            ,  
    '5400'    =>  t('90 minutes')        ,  
    '7200'    =>  t('2 hours')           ,  
    '10800'   =>  t('3 hours')           ,  
    '14400'   =>  t('4hours')            ,  
    '18000'   =>  t('5 hours')    ,  
    '21600'   =>  t('6 hours')    ,  
    '43200'   =>  t('12 hors')    ,  
    '86400'   =>  t('1 day')      ,  
    '172800'  =>  t('2 days')     ,  
    '259200'  =>  t('3 days')     ,  
    '345600'  =>  t('4 days')     ,  
    '432000'  =>  t('5 days')     ,  
    '518400'  =>  t('6 days')     ,  
    '604800'  =>  t('1 week')     ,  
    '1209600' =>  t('2 weeks')    ,  
    '2419200' =>  t('1 month')    ,  
  );

// TODO:
//  $form['type_configuration']['general']['pushpad_general_ttl'] = Array(
//    '#title' => t('Notification expire in'),
//    '#description' => t('Pushpad won\'t send notification to offline devices. This is the amount of time after which unsent notifications will be cancelled. This is usefull if you plan to send notification on a scheduled base'),
//    '#type' => 'select',
//    '#default_value' => variable_get('pushpad_general_ttl', ''),
//    '#options' => $options,
//    '#required' => FALSE,
//    '#weight'  => -98 ,
//  );            
      
    
    
  $form['type_configuration']['general']['description'] = array(
    '#type' => 'fieldset',
    '#title' => t('Description'),
  );    
    
  // Configuration option for adding a PUSHPAD description.
  $form['type_configuration']['general']['description']['pushpad_add_pushpad_description'] = array(
	  '#type' => 'checkbox',
	  '#title' => t('Add a description to the PUSHPAD'),
	  '#description' => t('Add a configurable description to explain the purpose of the PUSHPAD to the visitor.'),
	  '#default_value' => variable_get('pushpad_add_pushpad_description', TRUE),
  );
        
  // Textfield(s) for the PUSHPAD description.
  if (module_exists('locale')) {
	  $langs = locale_language_list();
	  $form['type_configuration']['general']['description']['pushpad_descriptions'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('PUSHPAD description'),
	  '#description' => t('Configurable description of the PUSHPAD. An empty entry will reset the description to default.'),
	  '#attributes' => array('id' => 'edit-pushpad-description-wrapper'),
	  );
	  foreach ($langs as $lang_code => $lang_name) {
	  $form['type_configuration']['general']['description']['pushpad_descriptions']["pushpad_description_$lang_code"] = array(
            '#type' => 'textfield',
            '#title' => t('For language %lang_name (code %lang_code)', array('%lang_name' => $lang_name, '%lang_code' => $lang_code)),
            '#default_value' => pushpad_get_description($lang_code),
            '#maxlength' => 256,
	  );
	  }
  }
  else {
    $form['type_configuration']['general']['description']['pushpad_descriptions']['pushpad_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Challenge description'),
    '#description' => t('Configurable description of the PUSHPAD. An empty entry will reset the description to default.'),
    '#default_value' => pushpad_get_description(),
    '#maxlength' => 256,
    '#attributes' => array('id' => 'edit-pushpad-description-wrapper'),
    );
  }    

    
  //  - B 
  // Render the Pro configuration.
  $form['type_configuration']['pro'] = array(
	'#type' => 'fieldset',
	'#title' => t('Pushpad Pro'),
  );
 
  $form['type_configuration']['pro']['pushpad_pro_main_project_id'] = Array(
	'#title' => t('your  project ID'),
	'#type' => 'textfield',
	'#default_value' => variable_get('pushpad_pro_main_project_id', ''),
	'#size' => 15,
	'#maxlength' => 12,
	'#required' => FALSE,
	'#description' => t('Provided by the PushPad Contron Panel. See !l', Array("!l" => l("https://pushpad.xyz/projects", "https://pushpad.xyz/projects")  )),
  );      
    
  //  - C 
  // Render the Express configuration.
  $form['type_configuration']['express'] = array(
	'#type'     => 'fieldset',
	'#title'    => t('Pushpad Express'),
	'#disabled' => true,
  );
  
  $form['type_configuration']['express']['pushpad_express_subscription_link'] = Array(
	  '#title' => t('Subscription link'),
	  '#type' => 'textfield',
	  '#default_value' => variable_get('pushpad_express_subscription_link', 'https://'),
	  '#size' => 15,
	  '#maxlength' => 40,
	  '#required' => FALSE,
	  '#description' => t('Provided by the PushPad Contron Panel.'),
  );  

  $form['type_configuration']['express']['pushpad_express_subscription_link_label'] = Array(
	  '#title' => t('Subscription link Label'),
	  '#type' => 'textfield',
	  '#default_value' => variable_get('pushpad_express_subscription_link_label', ''),
	  '#size' => 15,
	  '#maxlength' => 40,
	  '#required' => FALSE,
	  );        

  $form['type_configuration']['express']['pushpad_express_subscription_link_no_ui'] = Array(
	  '#title' => t('Subscription link without a UI'),
	  '#type' => 'textfield',
	  '#default_value' => variable_get('pushpad_express_subscription_link_no_ui', 'https://'),
	  '#size' => 15,
	  '#maxlength' => 40,
	  '#required' => FALSE,
	  '#description' => t('Provided by the PushPad Contron Panel.'),
  );    

  $form['type_configuration']['express']['pushpad_express_subscription_link_no_ui_label'] = Array(
	  '#title' => t('Subscription link without a UI Label'),
	  '#type' => 'textfield',
	  '#default_value' => variable_get('pushpad_express_subscription_link_no_ui_label', ''),
	  '#size' => 15,
	  '#maxlength' => 40,
	  '#required' => FALSE,
  );      
    
    
  if (variable_get('pushpad_active')== 0)  {
    drupal_set_message(t("Pushpad DISABLED on this website"), "warning", $repeat = FALSE);
  }
    
  return system_settings_form($form);
}


/**
 * Implements _form_validate().
 */
function pushpad_admin_settings_form_validate($form, &$form_state) {
  //  buggy drupal_set_message
  //  https://drupal.stackexchange.com/a/24111/13760
  $_SESSION['messages'] = '';
}

/**
 * Implements _form_submit().
 */
function pushpad_admin_settings_form_submit($form, &$form_state) {
}
