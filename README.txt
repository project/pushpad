Module: PushPad
Author: Augusto Fagioli <http://drupal.org/user/50680>


Description
===========
Adds the PushPad [https://pushpad.xyz/projects] notification system to your Drupal website.



Requirements
============
  *  a website served over HTTPS (we only support Pushpad Pro now)
  *  a Pushpad user account
  *  an active Sender created at https://pushpad.xyz/senders
  *  an active Project created at https://pushpad.xyz/projects



Installation
============
  *  copy the 'pushpad' module directory in the your Drupal sites/all/modules directory as usual.
  *  copy the 'pushpad-php' library in the  libraries directory. At the you will have  a  sites/all/libraries/pushpad-php directory



Usage
=====
  * Set up your preference at admin/config/services/pushpad



TODO
====
  *  Pushpad Express management
  *  A better handlig for //$options .=   "serviceWorkerPath: '".drupal_get_path('module', 'pushpad')."/service-worker.js',";
  *  Libraries implementation
  *  Documentation
  *  debug: Notice: Undefined index: tags in Pushpad\Notification->broadcast() (line 27 of ../sites/all/libraries/pushpad-php/lib/Notification.php).

  
  
Extra
=====
  * Pushpad morning notification module, which makes your website ready to reveice subscription and send notificaion in minutes. No coding involved.
